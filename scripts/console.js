// Generated by CoffeeScript 1.6.3
(function() {
  this.module('helpers', function() {
    return this.Console = (function() {
      function Console(elementId) {
        this.e = document.getElementById(elementId);
        this.basetime = new Date;
      }

      Console.prototype._constructP = function(s, c) {
        var p;
        s = this._msToString(new Date - this.basetime) + "" + s;
        p = document.createElement('p');
        p.appendChild(document.createTextNode(s));
        p.className = c;
        return p;
      };

      Console.prototype._padWithZero = function(s, t) {
        s = "" + s;
        return "" + (new Array(t + 1 - s.length).join('0')) + s;
      };

      Console.prototype._msToString = function(t) {
        var h, m, ms, s;
        ms = "" + (t % 1000);
        t = Math.floor(t / 1000);
        s = "" + (t % 60);
        t = Math.floor(t / 60);
        m = "" + (t % 60);
        t = Math.floor(t / 60);
        h = t;
        return "" + (this._padWithZero(h, 2)) + ":" + (this._padWithZero(m, 2)) + ":" + (this._padWithZero(s, 2)) + "." + (this._padWithZero(ms, 3)) + " > ";
      };

      Console.prototype.debug = function(s) {
        return this.e.appendChild(this._constructP(s, 'debug'));
      };

      Console.prototype.info = function(s) {
        return this.e.appendChild(this._constructP(s, 'info'));
      };

      Console.prototype.warning = function(s) {
        return this.e.appendChild(this._constructP(s, 'warning'));
      };

      Console.prototype.error = function(s) {
        return this.e.appendChild(this._constructP(s, 'error'));
      };

      return Console;

    })();
  });

}).call(this);
