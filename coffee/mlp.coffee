###
mlp.js
--------
Multilayer Perceptron based Neural Network using backpropagation
for weight adjustments.
###

class Neuron
  output: 0 
  weights: []
  uvalue: 0
  delta: null

  constructor: (prevLayer, transfer) ->
    @previousLayer = prevLayer      # The layer that provides input into this neuron
    @transfer = transfer        # The transfer function

    # initialise weights (including bias)
    if @previousLayer?
      @weights = (Math.random()*2 - 1 for [0...@previousLayer.nodes.length+1])

  # set the output of this node to a specific value
  setOutput: (o)->
    @output = o

  # calculate the output of this node based on its inputs from the previous layer
  calculate: ->
    if @previousLayer?
      input = [1].concat (l.output for l in @previousLayer.nodes)
      temp = (@weights[i] * input[i] for i in [0...input.length])
      @uvalue = temp.reduce (x,y)-> x + y
      @output = @transfer.eval(@uvalue)

    return @output

  applyWeightModifications: (mods) ->
    for m in [0...mods.length]
      @weights[m] += mods[m]

class SigmoidFunc
  @eval: (x) ->  return 1 / (1.0+Math.exp(-x))
  @derive: (x) -> return @eval(x) * (1 - @eval(x))


class MLPLayer
  constructor: (numnodes, prevLayer, tfunc) ->
    @nodes = []
    @previousLayer = prevLayer
    @transfer = tfunc
    @weightmods = null

    for [1..numnodes]
      @nodes.push new Neuron @previousLayer, @transfer

  calculate: ->
    if @previousLayer?
      for n in @nodes
        n.calculate()
    return @getOutputs()

  setOutputs: (outputs) ->
    for i in [0...outputs.length]
      @nodes[i].setOutput outputs[i]
    return true

  getOutputs: ->
    return (n.output for n in @nodes)

  calculateWeightModifications: (rate, targets, nextlayer) ->
    # Is this an output layer?
    if targets?
      @weightmods = []
      for i in [0...@nodes.length]
        node = @nodes[i]

        # d[i] = f'(u) (t-o)
        node.delta = @transfer.derive(node.uvalue) * (targets[i] - node.output)

        # calculate weight modifications from each pi->n
        mods = []
        mods.push rate * node.delta
        for pi in [0...@previousLayer.nodes.length]
          # delta w[ij] = rate (delta) * x[ij]
          mods.push rate * node.delta * @previousLayer.nodes[pi].output

        @weightmods.push mods

    # Is this a hidden layer
    else if nextlayer?
      @weightmods = []
      for i in [0...@nodes.length]
        node = @nodes[i]

        # summation thing
        sum = 0
        for n in nextlayer.nodes
          sum += n.weights[1+i] * n.delta

        d = @transfer.derive(node.uvalue) * sum

        mods = []
        mods.push rate * d
        for pi in [0...@previousLayer.nodes.length]
          mods.push rate * d * @previousLayer.nodes[pi].output
        @weightmods.push mods
    return true


  applyWeightModifications: ->
    if @weightmods?
      for n in [0...@nodes.length]
        @nodes[n].applyWeightModifications @weightmods[n]
    return true


class MLP
  trainingCallback: null

  constructor: (inum, lnums, onum, tfunc)->
    @hiddenLayers = []
    @outputLayer = []

    @inputLayer = new MLPLayer inum, null, tfunc
    last = @inputLayer

    for size in lnums
      h1 = new MLPLayer size, last, tfunc
      @hiddenLayers.push h1
      last = h1

    @outputLayer = new MLPLayer onum, last, tfunc

  setTrainingCallback: (func) ->
    @trainingCallback = func

  evaluate: (inputs) ->
    @inputLayer.setOutputs inputs

    for layer in @hiddenLayers
      layer.calculate()

    @outputLayer.calculate()

    return @outputLayer.getOutputs()

  # Mean Square error function
  test: (inputs, outputs) ->
    o = @evaluate inputs
    e = 0
    for i in [0...o.length]
      e += (outputs[i] - o[i]) * (outputs[i] - o[i])

    return e / 2.0

  train: (rate, inputs, targets) ->
    error = @test inputs, targets

    @outputLayer.calculateWeightModifications rate, targets, null
    lastlayer = @outputLayer
    for hi in [@hiddenLayers.length...0]
      @hiddenLayers[hi-1].calculateWeightModifications rate, null, lastlayer
      lastlayer.applyWeightModifications()
      lastlayer = @hiddenLayers[hi-1]

    lastlayer.applyWeightModifications()


    return error

  learn: (testcases, rate, maxiterations, targeterror) ->
    iterations = 0

    while iterations < maxiterations
      i = testcases.length
      while (--i > 0)
        j = ~~(Math.random() * (i + 1))
        t = testcases[j]
        testcases[j] = testcases[i]
        testcases[i] = t

      e = 0
      for c in testcases
        e += @train rate, c[0], c[1]

      if @trainingCallback?
        @trainingCallback e

      iterations++
      if e < targeterror
        break

    return [iterations, e]

class MainClass
  @run: ->

    consolebox = new helpers.Console 'consolebox'
    canvasbox = new helpers.CanvasContainer 'canvasbox' 
    canvasbox.setSize 1000,100

    bg = new graph.LineGraph canvasbox

    barDrawer = (error) =>
      bg.addPoint error
    
    m = new MLP 2, [2], 1, SigmoidFunc

    m.setTrainingCallback barDrawer

    testcases = [
      [[0,0],[0]],
      [[1,0],[1]],
      [[0,1],[1]],
      [[1,1],[0]]
    ]
    
    consolebox.info "Started training."

    r = m.learn testcases, 0.8, 10000, 0.01

    if r[1] > 0.01
      consolebox.error "Failed to reach the goal of e=0.001. Took #{r[0]} iterations. Final error: #{r[1]}"
    else
      consolebox.info "Trained XOR in #{r[0]} iterations. Final error: #{r[1]}"

    for c in testcases
      m.evaluate c[0]
      consolebox.info  "In: #{c[0]} Out: #{c[1]} ~ #{m.outputLayer.getOutputs()}"
    
    return 0

  @docprint: (s) ->
    n = document.createTextNode s
    p = document.createElement('p')
    p.appendChild n
    document.body.appendChild(p);

MainClass.run()


