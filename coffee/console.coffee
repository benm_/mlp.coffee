# CONSOLE Helper
# ==============
# Appends strings as paragraph elements to the div specified in the constructor
# Use CSS to add cool effects and customize the colours for each type.
#
# Time is the elapsed time since the console was created.
#
# Usage:
# <div id="consolebox"></div>
# 
# consolebox = new helpers.Console 'consolebox'
# consolebox.info 'Hello World'
#
# outputs: 00.00.00.000 > Hello World
# ===============================================================================

# helpers module
@module 'helpers', ->
  # Console class
  class @Console
    constructor: (elementId)->
      @e = document.getElementById elementId
      @basetime = new Date

    # construct the paragraph element
    _constructP: (s, c) ->
      s = @_msToString(new Date - @basetime) + "" + s
      p = document.createElement 'p'
      p.appendChild document.createTextNode s
      p.className = c
      return p

    _padWithZero: (s, t) ->
      s = "" + s
      return "#{new Array(t + 1 - s.length).join '0'}#{s}"

    # Convert a number of milliseconds to 00:00:00.000 format
    _msToString: (t) ->
      # calculate h m s ms
      ms = "#{t % 1000}"
      t = Math.floor (t / 1000)
      s = "#{t % 60}"
      t = Math.floor (t / 60)
      m = "#{t % 60}"
      t = Math.floor (t / 60)
      h = t

      return "#{@_padWithZero(h, 2)}:#{@_padWithZero(m, 2)}:#{@_padWithZero(s, 2)}.#{@_padWithZero(ms, 3)} > "

    debug: (s)->
      @e.appendChild @_constructP s, 'debug'

    info: (s)->
      @e.appendChild @_constructP s, 'info'

    warning: (s)->
      @e.appendChild @_constructP s, 'warning'

    error: (s)->
      @e.appendChild @_constructP s, 'error'
