@module 'graph', ->
  class @LineGraph
    points: []
    dataChanged: false
    maximum: 0.001

    constructor: (canvascontainer, barwidth) ->
      @cc = canvascontainer
      @color = 'rgb(255,0,0)'
      @sidePadding = 70
      @width = @cc.getWidth() - 2 * @sidePadding

      @cc.g.textBaseline="top";

    addPoint: (value) ->
      # Add the data point
      @points.push value
      
      if value > @maximum
        @maximum = value

      if not @dataChanged
        setTimeout @redraw, 16
        @dataChanged = true

      return true

    redraw: =>
      if @dataChanged
        @dataChanged = false
        # calculate drawable range
        w = @cc.getWidth()
        start = @points.length - w
        start = if start < 0 then 0 else start

        @cc.g.fillStyle = 'rgb(255,0,0)'

        d = @points.length / @width
        for i in [0...@width]
          n = Math.floor (d * i)
          v = @points[n]
          if @maximum > 0
            h = (v / @maximum) * @cc.getHeight()
            @cc.g.fillRect @sidePadding + i, @cc.getHeight() - h, 1, h

        @cc.g.textBaseline = "top"
        @cc.g.textAlign = "right"
        @cc.g.fillText "#{(@points[0]+"").substring 0, 10}", @sidePadding, 0

        lh = (@points[@points.length-1] / @maximum) * @cc.getHeight()

        @cc.g.textBaseline = "bottom"
        @cc.g.textAlign = "left"
        @cc.g.fillText "#{(""+@points[@points.length-1]).substring 0, 10}", @width + @sidePadding, @cc.getHeight() - lh

      return true





