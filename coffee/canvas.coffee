@module 'helpers', ->
  class @CanvasContainer
  	constructor: (elementId)->
      @e = document.getElementById elementId
      if @e.tagName != 'CANVAS'
      	throw "Element '##{elementId}' is not a canvas element!"
      @g = @e.getContext '2d'

    setSize: (w,h) ->
    	@e.width = w
    	@e.height = h

    getWidth: ->
    	@e.width

    getHeight: ->
    	@e.height


